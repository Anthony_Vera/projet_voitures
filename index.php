<?php
require_once "utils/functions.php";
require_once "utils/constants.php";
/** démarage de la session */
session_start();

/** lecture des données */
$json = file_get_contents(DB_VEHICULE); // lecture du fichier de données
$datas = json_decode($json, true); // transformation en tableau multidimensionel
$userJson = file_get_contents(DB_USER);
$datasUser = json_decode($userJson, true);
$errMsg = "";



if( isset( $_REQUEST['loginForm'] ) && $_REQUEST['loginForm'] == 1){ // demande de connexion
    $testUser = false;
    foreach ($datasUser as $user){
        if($_POST['login'] == $user['nom']){ // test existance utilisateur
            $testUser = true;
            if($user['password'] == md5($_POST['password'].CSRF)){
                $_SESSION['conected'] = true;
                $_SESSION['user'] = $user;
            } else {
                $errMsg =  "le mot de passe n'est pas bon";
            }
        }
    }
    if(!$testUser){
        $errMsg = "l'utilisateur n'existe pas";
    }
}
if( isset( $_REQUEST['logout'] ) && $_REQUEST['logout'] == 'ok' ){ // gestion de la déconnexion
    if( isset($_SESSION['conected']) && $_SESSION['conected']){
        $_SESSION = [];
        session_destroy();
    }
}

// récupération du post
if( isset( $_POST['model'] ) ){
    $voiture = [];
    $voiture['model'] = $_POST['model'];
    $voiture['marque'] = $_POST['marque'];
    $voiture['nbrPorte'] = $_POST['nbrPorte'];
    $voiture['image'] = $_POST['image'];

    if($_POST['formChoice'] == 'edition'){ // édition
        $datas[ $_POST['index'] ] = $voiture;
    } else { // creation
        $datas[] = $voiture;
    }
    file_put_contents("data/vehicules.json", json_encode($datas));
}
// effectuer une suppression
if( isset( $_REQUEST['delete']) ) {
    array_splice($datas, $_REQUEST['index'], 1);
    file_put_contents("data/vehicules.json", json_encode($datas));
}


$page = ( isset( $_REQUEST['page'] ) ) ? $_REQUEST['page'] : 'liste';
include "view/header.php";
 // définition de la page
switch ($page){ // switch de la vue selon le parametre
    case 'liste': // vue de base
        include "view/liste.php";
        break;
    case 'form':
        include "view/formulaire.php";
        break;
    case 'detail':
        include "view/detail.php";
        break;
    case 'register':
        include "view/register.php";
        break;
    case 'login':
        include "view/login.php";
        break;
}

include "view/footer.php";
