<?php

/**
 * afficherTitre
 *
 * @param  mixed $titre
 * @return void
 */
function showTitle( $title ) : void
{
    echo "<h2>" . $title . "</h2>";
}


/**
 * addHtmlElement
 * Ajout d'un champ html à la page
 * @param string $elementName
 * @param array $attributs
 * @param array $classNames
 * @param string|null $content
 * @return string
 */
function addHtmlElement( string $elementName,
                         array $attributs = [], array $classNames = [],
                         string $content = null ) : string
{
    $html = "";

    // ajout du nom de l'element
    $html .= "<" . $elementName;

    // ajout des classes de style
    if( !empty( $classNames ) ){
        $html .= " class='" . implode( " ", $classNames ) . "' ";
    }

    // ajout des attributs
    if( !empty( $attributs ) ){
        foreach($attributs as $key => $value){
            $html .= " " . $key . "='" . $value . "' ";
        }
    }

    $html .= ">" . $content . "</" . $elementName . ">";

    return $html;
}


/**
 * @param string $type
 * @param array $attr
 * @param array $classNames
 * @param string|null $label
 * @param bool $placeholder
 * @param array $arg
 * @return string
 */
function addFieldForm(string $type, array $attr = [],
                      array $classNames = [], string $label = null,
                      bool $placeholder = false, array $arg = []) : string
{
    $field = "<div class='fields " .
        ( ( $type == 'checkbox' || $type == 'radio' ) ? ' grouped ' : '' ) .
        "'>"; // definition du wrapper de champs de formulaire

    $id = (array_key_exists('id', $attr))?($attr['id']):("form".rand(0,100)); // protection contre l'oublie d'id

    if(!$placeholder && $type != "submit"){ // si pas de place holder et pas de type submit
        $field .= "<label for='" . $id . "'>" . $label . "</label>";
    }

    $field .= ($type == 'textarea') ? "<textarea id='" . $id . "'" : ( ( $type == 'checkbox' || $type == 'radio' ) ? "" : "<input  id='" . $id . "'" );

    $attributs = ""; // déclaration de la variable $attributs
    if( !empty( $attr ) ){ // si  il y a des attributs
        foreach( $attr  as $key => $value){ // parcours des attributs
            if( $key != 'id' ||
                ( $key == "value" &&
                    ( $type == 'radio' || $type == 'checkbox' ) ) ) // on exclut id que l'on a déjà traité
                $attributs .= " " . $key . '="' . $value .'" ';

        }
    }

    $class = "";
    if( !empty($classNames) ){ // si il y a des class
        $class .= " class='" . implode(" ", $classNames) . "' ";
    }

    switch ($type) { // selon le type de champ
        case 'text':
        case 'number':
        case 'password':
        case 'email':
        case 'hidden':
        case 'file':
            $field .= " type='" .$type . "' ";
            $field .= ($placeholder) ? " placeholder='" . $label . "' ": "";
            $field .= $attributs.$class;
            $field .= " />";
            break;

        case 'textarea': // cas du textarea
            $field .= $attributs.$class;
            $field .= ($placeholder) ? " placeholder='" . $label . "' >": " >";
            $field .= "</textarea>";
            break;
        case 'radio':
        case 'checkbox':
            foreach($arg as $key => $value){ // parcour des arguments de ceck/radio
                $field .= "<div class='field'>";
                $field .= "<div class='ui " . ( ( $type == 'radio' ) ? " radio " : "" ) . " checkbox'>"; // class sementic pour check/radio
                $field .= "<input type='" .$type . "' "; // créa de l'input
                $field .= " value='" . $value . "' "; // valeur de l'input
                $field .= $attributs.$class;
                $field .= ( ( $attr['value'] == $value )? ' checked ' : '' )." />";
                $field .= "<label>" . $key . "</label></div></div>"; // label de l'input
            }
            break;
        case 'submit':
            $field .= " type='" .$type . "' ";
            $field .= $attributs.$class;
            $field .= " />";
            break;
    }

    $field .= "</div>";
    return $field;
}

/**
 * @param array $elements
 * @return string
 */
function genereTable(array $elements = []) : string
{
    $table = "";
    // generer l'entête
    $cellTh = "";
    foreach($elements[0] as $entete){ // paracour la ligne 0 du tableau
        $cellTh .= addHtmlElement('th', [], [],$entete);
    }
    $ligneEntete = addHtmlElement('tr',[],[], $cellTh);
    $table .= addHtmlElement('thead',[],[], $ligneEntete);

    // genere le body du tableau
    $ligne = "";
    foreach($elements as $key => $line){ // on parcourt les lignes du tableau principal
        if( $key != 0 ){
            $td = "";
            foreach($line as $value){ // on parourt les valeurs du sous tableau
                $td .= addHtmlElement('td', [], [], $value);
            }
            $ligne .= addHtmlElement('tr', [], [], $td);
        }
    }
    $table .= addHtmlElement('tbody',[],[], $ligne);
    return addHtmlElement('table', [], ['ui','table','celled'], $table);
}


/**
 * genereForm
 *
 * @param  mixed $formulaire
 * @param  mixed $action
 * @return string
 */
function genereForm(array $formulaire = [], string $action) : string
{
    $form = "";
    foreach( $formulaire as $field ){
        $form .= addFieldForm($field['type'], $field['attr'],
            $field['class'], $field['label'],
            $field['ph'], $field['args']);
    }

    return addHtmlElement('form',
        ['id'=>'myForm', 'method'=>'post', 'action'=> $action],
        ['ui','form'], $form);
}

/**
 * @param array $car
 * @return string
 */
function addCard(array $car, $index = -1) : string
{
    $cardContent = "";
    // affichage de l'image
    $cardContent .= addHtmlElement("div",[],['image'],
        addHtmlElement('img',[ 'src' => 'img/' . $car['image'] ],['ui','fluid','image'],''));
    $body = "";
    $body .= addHtmlElement('h4',[],[], $car['model']); // affichage du model
    $body .= addHtmlElement('p',[],[], $car['marque']); // affichage de la marque
    $body .= addHtmlElement('p',[],[],
        "Nombre de portes : ".$car['nbrPorte']); // affichage du nbr de porte

    /**
     * gestion des boutons du card
     */
    if ( isConnected() ) {
        $btns = "";
        // bouton voir
        $bodyform = addFieldForm('hidden', ['name' => 'page', 'value' => 'detail'], [],
            null, false, []);
        $bodyform .= addFieldForm('hidden', ['name' => 'index', 'value' => $index], [],
            null, false, []);
        $bodyform .= addHtmlElement('button', ['type' => 'submit'], ['ui', 'icon', 'button'],
            '<i class="icon eye"></i>');
        $btns .= addHtmlElement('form',
            ['method' => 'post', "action" => 'index.php'], [], $bodyform);

        if(isAdmin()) { // affichage des boutons réservé à l'admin
            // bouton delete
            $bodyform = addFieldForm('hidden', ['name' => 'delete', 'value' => 'ok'], [],
                null, false, []);
            $bodyform .= addFieldForm('hidden', ['name' => 'index', 'value' => $index], [],
                null, false, []);
            $bodyform .= addHtmlElement('button', ['type' => 'submit'], ['ui', 'icon', 'button'],
                '<i class="icon trash alternate"></i>');
            $btns .= addHtmlElement('form',
                ['method' => 'post', "action" => 'index.php'], [], $bodyform);

            // bouton update
            $bodyform = addFieldForm('hidden', ['name' => 'page', 'value' => 'form'], [],
                null, false, []);
            $bodyform .= addFieldForm('hidden', ['name' => 'index', 'value' => $index], [],
                null, false, []);
            $bodyform .= addHtmlElement('button', ['type' => 'submit'], ['ui', 'icon', 'button'],
                '<i class="icon edit"></i>');
            $btns .= addHtmlElement('form',
                ['method' => 'post', "action" => 'index.php'], [], $bodyform);
        }
        $body .= addHtmlElement('div', [], ['extra', 'content', 'buttonFlex'], $btns);
    }
    $cardContent .= addHtmlElement('div', [],['content'], $body);
    return addHtmlElement("div", [], ['ui','card','margin'], $cardContent);
}

/**
 * @return bool
 */
function isConnected() : bool
{  // on retourne le résultat du test de connection
    return ( isset( $_SESSION['conected'] ) && $_SESSION['conected'] );
}

function isAdmin() : bool
{
    return ($_SESSION['user']['role'] == 'admin');
}
