<?php

$vehicule = $datas[ $_REQUEST['index'] ];
?>
<div class="ui placeholder segment">
    <div class="ui two column very relaxed stackable grid">
        <div class="column">
            <img src="img/<?php echo $vehicule['image']; ?>" style="width: inherit" />
        </div>
        <div class="middle aligned column">
            <h4><?php echo $vehicule['model']; ?></h4>
            <p><?php echo $vehicule['marque']; ?></p>
            <p>nombre de portes : <?php echo $vehicule['nbrPorte']; ?></p>
        </div>
    </div>
    <div class="ui vertical divider">
    </div>
</div>
