<?php
$voiture = ["model"=>"", "marque" => '', "nbrPorte"=>"", "image" => ""];
(isset( $_REQUEST['index'] ) ) ? $voiture = $datas[ $_REQUEST['index'] ] : '';
$form = [
    ['type' => 'text', 'attr' => ["name"=>"model", "value" => $voiture['model'] ], 'class'=>[],
        'label' => "model du véhicule", 'ph' => true, 'args' => []],
    ['type' => 'text', 'attr' => ["name"=>"marque", "value" => $voiture['marque'] ], 'class' => [],
        'label' => "marque", 'ph' => true, 'args' => []],
    ['type' => 'radio', 'attr' => ["name"=>"nbrPorte", "value" => $voiture['nbrPorte'] ], 'class' => [],
        'label' => "nombre de porte", 'ph' => false, 'args' => ["3 portes" => 3, "5 portes" => 5]],
    ['type' => 'text', 'attr' => ["name"=>"image", "value" => $voiture['image'] ], 'class' => [],
        'label' => "image", 'ph' => true, 'args' => []],
    ['type' => 'hidden', 'attr' => ["name"=>"formChoice",
        "value" => ( isset( $_REQUEST['index'] ) ) ? 'edition' : 'creation' ], 'class'=>[],
        'label' => "", 'ph' => false, 'args' => []],
    ['type' => 'hidden', 'attr' => ["name" => "index",
        "value" => ( isset( $_REQUEST['index'] ) ) ? $_REQUEST['index'] : -1 ], 'class'=>[],
        'label' => "", 'ph' => false, 'args' => []],
    ['type' => 'submit', 'attr' => ["value"=>"Enregister"], 'class' => ['ui','button'],
       'label' => null, 'ph' => false, 'args' => []],
];

$f = genereForm($form, 'index.php');
echo addHtmlElement('div',[],['ui', 'segment'], $f);
