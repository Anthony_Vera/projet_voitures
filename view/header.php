<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no,
          initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
    <style>
        .flexy {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            flex-wrap: wrap;
            align-items: stretch;
            align-content: flex-start;
        }
        .margin {
            margin-left: 10px !important;
            margin-top: 0px !important;
            margin-bottom: 20px !important;
        }
        .buttonFlex {
            display: flex;
            flex-direction: row;
            justify-content: center;
        }
    </style>
</head>
<body class="ui container">
<?php
    $liste = $form = $reg = "";
    switch ($page){
        case 'liste':
            $liste = 'active';
            break;
        case 'form':
            $form = 'active';
            break;
        case 'register':
            $reg = 'active';
            break;
    }
//    var_dump($_SESSION);
?>
<div class="">
    <div class="ui <?php echo (isConnected() && isAdmin()) ? 'four' : 'three'; ?> item menu">
        <a href="index.php" class="<?php echo $liste ?> item">Accueil</a>
        <?php if(isConnected() && isAdmin()){ ?>
        <a href="index.php?page=form"
           class="<?php echo ($page == 'form') ? 'active' : '' ?> item">Ajouter</a>
        <?php } ?>
        <a href="index.php?page=register"
           class="<?php echo ($page == 'register') ? 'active' : '' ?> item">Inscription</a>

        <?php
        if( isConnected() ){
            echo '<a href="index.php?logout=ok&page=liste" class="item">Se déconnecter</a>';
        } else {
            echo '<a href="index.php?page=login" class="'. (
            ($page == "login") ? "active" : "" ). ' item">Se connecter</a>';
        }
        ?>
    </div>
</div>



<?php
if(strlen($errMsg) > 0){ // affichage du message d'erreur
    echo addHtmlElement("div",[],['ui','segment','danger'], $errMsg);
}
if( isConnected() ){
    $msg = "bienvenue ". $_SESSION['user']['nom'] . " " . $_SESSION['user']['prenom'];
    echo addHtmlElement("div",[],['ui','segment'], $msg);
}
