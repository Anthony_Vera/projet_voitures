<?php

$loginForm = [
  [
      'type'=>'text', 'attr'=>['name'=>'login'],'class'=>[],"ph"=>true
      ,'label'=>'Identifiant', 'args'=> []
  ],
  [
      'type'=>'password', 'attr'=>['name'=>'password'],'class'=>[],"ph"=>true
      ,'label'=>'Mot de passe', 'args'=> []
  ],
    [
      'type'=>'hidden', 'attr'=>['name'=>'loginForm', "value"=>1],'class'=>[],"ph"=>false
      ,'label'=>'', 'args'=> []
  ],
  [
      'type'=>'submit', 'attr'=>['value'=>'Connexion'],'class'=>[],"ph"=>false
      ,'label'=>'', 'args'=> []
  ],
];

echo addHtmlElement("div", [], ['ui','segment'], genereForm($loginForm, 'index.php'));
