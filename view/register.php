<?php
//echo (isset($_POST['nom'])) ? $_POST['nom'] : '';
$date = new DateTime();

if( isset($_POST['nom']) ) { // enregistrement du l'utilisateur

    if($_POST['CSRF'] == md5($date->format('Y-m-d').CSRF)) {
        $user = []; // tableau des proprietés de l'utilisateur
        $user['nom'] = $_POST['nom'];
        $user['prenom'] = $_POST['prenom'];
        $user['login'] = $_POST['login'];
        $user['role'] = $_POST['role'];

        $user['password'] = md5($_POST['password']. CSRF);

        array_push($datasUser, $user); // on ajoute au tableau globals des utilisateurs
        file_put_contents(DB_USER, json_encode($datasUser));
    } else {
        echo "vous êtes un pirate !!!!";
    }
}



$regForm = [
    [ // champs nom
        'type' => 'text', 'attr' => [ 'name' => 'nom' ], 'class' => [],
        'ph' => true, 'label' => 'Nom', 'args' => []
    ],
    [ // champ prenom
        'type' => 'text', 'attr' => [ 'name' => 'prenom' ], 'class' => [],
        'ph' => true, 'label' => 'Prenom', 'args' => []
    ],
    [ // champ identifiant
        'type' => 'text', 'attr' => [ 'name' => 'login' ], 'class' => [],
        'ph' => true, 'label' => 'Login', 'args' => []
    ],
    [ // mot de passe
        'type' => 'password', 'attr' => [ 'name' => 'password' ], 'class' => [],
        'ph' => true, 'label' => 'Mot de passe', 'args' => []
    ],
    [  // role admin / user
        'type' => 'radio', 'attr' => [ 'name' => 'role', 'value' => 'user' ], 'class' => [],
        'ph' => true, 'label' => 'Role',
        'args' => ['Administrateur'=>'admin', 'Utilisateur' => 'user']
    ],
    [ // argument de redirestion
        'type' => 'hidden', 'attr' => [ 'name' => 'page', 'value'=>'register' ],
        'class' => [],
        'ph' => true, 'label' => '', 'args' => []
    ],
    [
        'type' => 'hidden',
        'attr' => [ 'name' => 'CSRF',
            'value'=>  md5( $date->format('Y-m-d').CSRF )],
        'class' => [],
        'ph' => true, 'label' => 'Nom', 'args' => []
    ],
    [ // submit
        'type' => 'submit', 'attr' => [ 'value' => "S'inscrire" ],
        'class' => [],
        'ph' => false, 'label' => '', 'args' => []
    ],
];

echo addHtmlElement('div', [],
    ['ui','segment'], genereForm($regForm, 'index.php'));
